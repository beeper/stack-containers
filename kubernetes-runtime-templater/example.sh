#!/bin/bash

jq -c . <<EOF > output.json
{"sts":$(kubectl -n synapse get sts -o json)}
EOF

python3 jinja2-cli.py "$1" output.json
