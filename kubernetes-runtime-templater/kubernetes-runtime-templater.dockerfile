# vim:set ft=dockerfile:

FROM debian:buster-slim

RUN apt-get update && \
	apt-get install -y curl python3 python3-jinja2 python3-yaml python3-click jq \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Installed for purpose of loading data from Kubernetes
RUN mkdir -p /usr/local/bin \
	&& curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
	&& chmod u+x kubectl \
	&& mv kubectl /usr/local/bin

ADD jinja2-cli.py /

ENTRYPOINT ["/jinja2-cli.py"]
