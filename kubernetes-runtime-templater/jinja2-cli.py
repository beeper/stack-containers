#!/usr/bin/env python3
# pylint: disable=invalid-name,missing-module-docstring

import os
import json

import yaml
import click
from jinja2 import Environment, FileSystemLoader, select_autoescape


@click.command()
@click.argument("template", type=click.Path(exists=True))
@click.argument("output", type=click.File('w'))
@click.option("--values", multiple=True, required=False, type=click.File('r'))
def main(template, output, values):
    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )
    env.globals['env'] = os.environ

    yaml_loader = yaml.CLoader

    for values_file in values:
        if values_file.name.endswith("yaml"):
            env.globals.update(yaml.load(values_file, Loader=yaml_loader))
        elif values_file.name.endswith("json"):
            env.globals.update(json.load(values_file))
        else:
            raise Exception("Unsupported data format (expected JSON or YAML)",
                            values_file.name)

    template_loader = env.get_template(template)
    with output:
        output.write(template_loader.render())
        # It REALLY should not need this. I don't know why it does.
        output.write("\n")


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
