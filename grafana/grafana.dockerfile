FROM grafana/grafana:8.4.3
RUN grafana-cli plugins install camptocamp-prometheus-alertmanager-datasource 1.0.0
