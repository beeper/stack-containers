# vim:set ft=dockerfile:

FROM debian:buster-slim

RUN apt-get update && apt-get install -y awscli curl zsh

RUN mkdir -p /usr/local/bin \
	&& curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
	&& chmod u+x kubectl \
	&& mv kubectl  /usr/local/bin

ADD postgres-backup.zsh /

CMD ["/postgres-backup.zsh"]
