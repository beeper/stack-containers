#!/bin/zsh

# Required environment variables:
# AWS_SECRET_ACCESS_KEY
# AWS_ACCESS_KEY_ID
# S3_BUCKET_REGION
# S3_BUCKET_NAME
#
# Optional environment variables:
# TEMP_OUTPUT_FILE
#
# Requires a ServiceAccount with `get pod` and `exec` permissions.

TEMP_OUTPUT_FILE="${TEMP_OUTPUT_FILE:-output.sql}"

BACKUP_PREFIX="$(date +%Y%m%d)"

log() {
  printf "ts=$(date +%s) "
  for item text in "$@"; do
    printf "$item=\"$text\" "
  done
  echo
}

info() {
  log level info "$@"
}

warning() {
  log level warning "$@"
}

error() {
  log level error "$@"
}

aws_s3_cp() {
  aws s3 cp --storage-class STANDARD_IA $@
}


info canary aws message "running canary to ensure configuration works"
canary="$(mktemp)"
echo "canary" > $canary
aws_s3_cp --dryrun "$canary" "s3://$S3_BUCKET_NAME/canary"
exit_code="$?"
if [ ! "$exit_code" -eq 0 ]; then
  error canary aws message "canary failed: dry run failed"
  exit 1
fi

aws_s3_cp --quiet "$canary" "s3://$S3_BUCKET_NAME/canary"
exit_code="$?"
if [ ! "$exit_code" -eq 0 ]; then
  error canary aws message "canary failed: upload failed"
  exit 1
fi
info canary aws message "canary succeeded"

info canary kubectl message "running canary to ensure configuration works"
kubectl -n kube-system get pod -l k8s-app=kube-dns
exit_code="$?"
if [ ! "$exit_code" -eq 0 ]; then
  error canary kubectl message "canary failed: kubectl get pod"
  exit 2
fi

pod="$(kubectl -n kube-system get pod -l k8s-app=kube-dns | tail -1 | cut -d' ' -f1)"
kubectl -n kube-system exec "$pod" -- /coredns -version
exit_code="$?"
if [ ! "$exit_code" -eq 0 ]; then
  error canary kubectl message "canary failed: kubectl exec"
  exit 2
fi
info canary kubectl message "canary succeeded"

info message "starting backup for date: $BACKUP_PREFIX"
kubectl --all-namespaces=true get pods -l backup=postgres | tail -n +2 | while read namespace pod rest; do
  info namespace "$namespace" pod "$pod" message "found pod to back up"
  dbs=$(kubectl -n "$namespace" exec "$pod" -- /bin/sh -c 'echo "SELECT datname FROM pg_database" | psql -tA -U "${POSTGRES_USER:-postgres}"')
  info namespace "$namespace" pod "$pod" message "found $(echo $dbs | wc -l) databases inside the pod"
  for db in ${=dbs}; do
    rm -f "${TEMP_OUTPUT_FILE}"
    info namespace "$namespace" pod "$pod" database "$db" message "starting dump"
    kubectl -n "$namespace" exec "$pod" -- /bin/sh -c 'pg_dump -O -U "${POSTGRES_USER:-postgres}" '$db | gzip -6 > "$TEMP_OUTPUT_FILE"
    exit_code="$?"
    if [ ! "$exit_code" -eq 0 ]; then
      error namespace "$namespace" pod "$pod" database "$db" exit_code "$exit_code" message "kubectl command failed"
      continue
    fi
    info namespace "$namespace" pod "$pod" database "$db" message "backup size is $(wc -c "$TEMP_OUTPUT_FILE" | cut -d' ' -f1) bytes"
    info namespace "$namespace" pod "$pod" database "$db" message "dry run: uploading backup to S3"
    aws_s3_cp --dryrun "$TEMP_OUTPUT_FILE" "s3://${S3_BUCKET_NAME}/${BACKUP_PREFIX}.${namespace}.${pod}.${db}.sql.gz"
    exit_code="$?"
    if [ ! "$exit_code" -eq 0 ]; then
      error namespace "$namespace" pod "$pod" database "$db" exit_code "$exit_code" message "aws s3 dryrun command failed"
      continue
    fi
    info namespace "$namespace" pod "$pod" database "$db" message "uploading backup to S3"
    aws_s3_cp --quiet "$TEMP_OUTPUT_FILE" "s3://${S3_BUCKET_NAME}/${BACKUP_PREFIX}.${namespace}.${pod}.${db}.sql.gz"
    exit_code="$?"
    if [ ! "$exit_code" -eq 0 ]; then
      error namespace "$namespace" pod "$pod" database "$db" exit_code "$exit_code" message "aws s3 command failed"
      continue
    fi
  done
done

info message "done!"

rm -f "${TEMP_OUTPUT_FILE}"
