FROM --platform=linux/amd64 debian:buster-slim

ARG BUILD_PACKAGES=' \
    curl build-essential libwrap0-dev \
'

ARG DANTE_VERSION='1.4.3'
ARG DANTE_CONFIGURE_ARGS=' \
    --without-ldap \
    --without-upnp \
    --without-bsdauth \
    --without-gssapi \
    --without-pam \
    --without-sasl \
    --without-pac \
    --disable-client \
'

# Download & compile dante
RUN apt-get update \
    && apt-get install -y $BUILD_PACKAGES \
    && curl https://www.inet.no/dante/files/dante-$DANTE_VERSION.tar.gz | tar -xz \
    && cd dante-$DANTE_VERSION \
    # See https://lists.alpinelinux.org/alpine-devel/3932.html
    && ac_cv_func_sched_setscheduler=no ./configure $DANTE_CONFIGURE_ARGS \
    && make \
    && make install \
    && apt-get remove -y $BUILD_PACKAGES \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Add user for sockd
RUN adduser --system --disabled-password --disabled-login --no-create-home sockd

COPY sockd.conf /etc/sockd.conf

ENTRYPOINT ["sockd"]
