FROM python:3.7-slim-buster

RUN apt-get update \
    && apt-get install -y build-essential libpq-dev gettext-base git \
    && pip install \
        pipelinewise-tap-postgres==1.8.1 \
        git+https://github.com/bradtgmurray/pipelinewise-target-bigquery@add-activate-version-support  \
    && apt-get purge -y build-essential \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Disable stream refresh on start, instead we do this by hand in the run-etl.sh script, resolves:
# https://github.com/transferwise/pipelinewise-tap-postgres/pull/129
RUN sed -i \
    's/^    refresh_streams_schema/    #refresh_streams_schema/' \
    /usr/local/lib/python3.7/site-packages/tap_postgres/__init__.py

COPY ./apply-replication-config.py ./
COPY ./run-etl.sh ./

CMD ["run-etl.sh"]
