#!/usr/bin/env python3

import sys
import json


def apply_replication_config(properties_filename, replication_config_filename):
    with open(properties_filename, 'r') as f:
        properties = json.load(f)

    with open(replication_config_filename, 'r') as f:
        replication_config = json.load(f)

    tables_to_sync = replication_config['tables_to_sync']
    table_columns_to_ignore = replication_config.get('table_columns_to_ignore', {})

    for stream in properties['streams']:
        table_name = stream['table_name']
        if table_name in tables_to_sync:
            stream['metadata'][0]['metadata']['selected'] = True
            stream['metadata'][0]['metadata'].update(tables_to_sync[table_name])

            if 'schema_overrides' in replication_config and table_name in replication_config['schema_overrides']:
                table_schema_overrides = replication_config['schema_overrides'][table_name]

                for item in stream['metadata']:
                    if (
                        len(item['breadcrumb']) == 2
                        and item['breadcrumb'][0] == 'properties'
                        and item['breadcrumb'][1] in table_schema_overrides
                    ):
                        item['metadata'].update(table_schema_overrides[item['breadcrumb'][1]])


    print(json.dumps(properties, indent=4))


if __name__ == '__main__':
    properties_filename = sys.argv[1]
    replication_config_filename = sys.argv[2]
    apply_replication_config(properties_filename, replication_config_filename)
