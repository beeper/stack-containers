#!/bin/bash

set -euo pipefail

templatesDir=${TEMPLATES_DIR:-/templates}
stateDir=${STATE_DIR:-/state}

stateFile=$stateDir/state.json
liveStateFile=$stateDir/live-state.json

function commitAnyLiveState() {
    # If we have some content, take the last line as state
    if [ -s $liveStateFile ]; then
        tail -n 1 $liveStateFile > $stateFile
    fi
    rm -f $liveStateFile
}

# If we have live state, that means we probably died mid-ETL run, so use the
# latest state from that file for this run, meaning we don't re-ETL the same
# data over and over.
if [ -f $liveStateFile ]; then
    commitAnyLiveState
fi

# No state or live state, bootstrap with empty state
if [ ! -s $stateFile ]; then
    echo "{}" > $stateFile
fi

envsubst < $templatesDir/config-bigquery.template.json > config-bigquery.json
envsubst < $templatesDir/config-postgres.template.json > config-postgres.json

tap-postgres --config config-postgres.json --discover > properties.json

# Apply replication config to the discovered schema
./apply-replication-config.py properties.json $REPLICATION_CONFIG_FILENAME > replication-properties.json

tap-postgres \
    --config config-postgres.json \
    --properties replication-properties.json \
    --state $stateFile | \
    target-bigquery \
        --config config-bigquery.json >> \
        $liveStateFile

# Push the final state line into the persistent state file for the next run
commitAnyLiveState
