FROM debian:bullseye-slim

RUN apt-get update && \
    apt-get install -y openssl python3 python3-boto3 python3-redis && \
    rm -rf /var/lib/apt/lists/*
