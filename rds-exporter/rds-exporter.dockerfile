FROM --platform=linux/amd64 debian:buster-slim

RUN apt-get update \
    && apt-get install -y curl \
    && curl -fsSL https://github.com/percona/rds_exporter/releases/download/v0.7.1/rds_exporter_linux_amd64.tar.gz -o /tmp/rds_exporter.tar.gz \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp
RUN tar -xf /tmp/rds_exporter.tar.gz

RUN cp /tmp/rds_exporter /usr/local/bin/rds_exporter
