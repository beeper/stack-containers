# Build container
FROM rust:1.54-slim-buster as builder

RUN apt-get update
RUN apt-get install -y git build-essential pkg-config libssl-dev

RUN apt-get install -y python3 python3-boto3

RUN git clone --branch fix-ratio-check https://github.com/fizzadar/rust-synapse-compress-state.git \
    /opt/synapse-compressor

WORKDIR /opt/synapse-compressor

RUN cargo build

WORKDIR /opt/synapse-compressor/synapse_auto_compressor/

RUN cargo build

# Execute container
FROM debian:buster-slim

RUN apt-get update && apt-get install -y openssl postgresql-client python3 python3-boto3

COPY --from=builder /opt/synapse-compressor/target/debug/synapse_compress_state /usr/local/bin/synapse_compress_state
COPY --from=builder /opt/synapse-compressor/target/debug/synapse_auto_compressor /usr/local/bin/synapse_auto_compressor
